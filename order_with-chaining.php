<?php

class Order {
    protected $purchasePrice = 0;
    protected $cart = [];
    protected $coupons = [];
    protected $taxRate = 0;
    protected $purchaseTime;

    // Add item to cart
    public function addItem($item, $price) {
        $this->cart[] = [
            'item' => $item,
            'price' => floatval($price),
        ];
        return $this;
    }

    // Add a coupon to the order
    public function addCoupon($code, $value) {
        $this->coupons[] = [
            'code' => $code,
            'value' => floatval($value),
        ];
        return $this;
    }

    // Apply tax to price
    public function tax($rate) {
        $this->taxRate = $rate;
        return $this;
    }

    // Finish the order
    public function purchase() {
        // Get total for items, minus coupon discounts, plus tax
        $this->purchasePrice = $this->subtotal() + $this->taxAmount();

        // Process credit card, etc here...
        // ...

        $this->purchaseTime = time();
        return $this;
    }

    // Send email notification
    public function notify($emailAddress) {
        // Logic to email order details here...
        // ...
        return $this;
    }

    // Get the subtotal (item costs, minus coupon values)
    protected function subtotal() {
        return $this->debitTotal() - $this->creditTotal();
    }

    // Get the tax amount (subtotal plus applicable tax)
    protected function taxAmount() {
        return $this->subtotal() * $this->taxRate;
    }

    // Get total for all items in the cart
    protected function debitTotal() {
        $total = 0;

        // Add up item prices
        foreach($this->cart as $i) {
            $total += $i['price'];
        }

        return $total;
    }

    // Get total for all coupons
    protected function creditTotal() {
        $total = 0;

        // Add up coupon values
        foreach($this->coupons as $i) {
            $total += $i['value'];
        }

        return $total;
    }

    // Get details for the order
    public function summary() {
        return [
            'purchase_price' => $this->purchasePrice,
            'subtotal' => $this->subtotal(),
            'tax_amount' => $this->taxAmount(),
            'items' => $this->cart,
            'coupons' => $this->coupons,
            'purchased_at' => $this->purchaseTime,
        ];
    }
}


// Create the new order
$order = new Order();

$summary = $order->addItem('pizza', 15)  // Add items
    ->addItem('soda', 2.5)
    ->addItem('cookie', 1)
    ->addCoupon('5-OFF', 5)             // Apply a discount
    ->tax(.06)                                 // Set 6% sales tax
    ->purchase()                                    // Purchase items
    ->notify('customer@example.com')    // Send email notification
    ->summary();                                    // Get order summary

echo '<pre>'.print_r($summary, true).'</pre>';